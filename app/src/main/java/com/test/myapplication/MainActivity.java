package com.test.myapplication;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.google.common.annotations.VisibleForTesting;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends Activity {

    @VisibleForTesting
    public static final String FOOTER = "FOOTER";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Deckard");

        List<String> values = new ArrayList<>();
        for(int i=0; i<100; i++) {
            values.add("item:" + i);
        }

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_list_item_1, android.R.id.text1, values);

        ListView listView = (ListView) findViewById(R.id.list);
        listView.setAdapter(adapter);

        LayoutInflater layoutInflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        final View footerView = layoutInflater.inflate(R.layout.footer_view, null);
        listView.addFooterView(footerView, FOOTER, false);
    }
}
